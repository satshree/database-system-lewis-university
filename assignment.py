import sys
import getpass
from pymysql import connect, cursors
from datetime import datetime, date
from dateutil.parser import parse

__author__ = "Satshree Shrestha"

THIS_YEAR = datetime.now().year


class AddressBook:
    """Address Book Application."""

    people = []
    address = []
    people_address = []

    def connect(self, host=None, user=None, password=None, database=None):
        """Connect to MySQL Database."""
        try:
            if host:
                self.host = host

            if user:
                self.user = user

            if database:
                self.database = database

            self.connection = connect(
                host=self.host,
                user=self.user,
                password=password,
                database=self.database,
                charset='utf8mb4',
                cursorclass=cursors.DictCursor
            )
        except Exception as e:
            print("-" * 75)
            print("Error:", str(e))
            print("-" * 75)
            sys.exit(1)
        else:
            self.fetch_people()
            self.fetch_address()
            self.fetch_people_address()

        return True

    def set_host(self, host):
        """Set host for database."""
        self.host = host

    def set_database(self, database):
        """Set database name."""
        self.database = database

    def set_user(self, user):
        """Set user for database."""
        self.user = user

    def fetch_people(self):
        """Fetch people_master data."""

        # SQL STATEMENT
        sql = """
        SELECT
            *
        FROM
            people_master
        """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

            # SET PEOPLE DATA
            self.people = cursor.fetchall()

        return self.people

    def fetch_address(self):
        """Fetch addresses data."""

        # SQL STATEMENT
        sql = """
        SELECT
            *
        FROM
            addresses
        """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

            # SET ADDRESS DATA
            self.address = cursor.fetchall()

        return self.address

    def fetch_people_address(self):
        """Fetch people_address data."""

        # SQL STATEMENT
        sql = """
        SELECT
            *
        FROM
            people_address
        """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

            # SET PEOPLE ADDRESS DATA
            self.people_address = cursor.fetchall()

        return self.people_address

    def search_people(self, value=None):
        """Search people contact information."""
        result = []
        for people in self.people:
            if value in people["person_name"]:
                result.append(people)

        return result

    def get_address(self, street_address=None, city=None, state=None, zip_code=None):
        """Get address information."""
        for address in self.address:
            if street_address == address["street_address"] and city == address["city"] and state == address["state"] and zip_code == str(address["zip_code"]):
                return address

        return None

    def get_people(self, person_name=None, person_DOB=None, active_phone_number=None):
        """Get people contact information."""

        # PARSE DATE OF BIRTH
        if type(person_DOB) == str:
            person_DOB = parse(person_DOB).strftime("%Y-%m-%d")
        elif type(person_DOB) in (date, datetime):
            person_DOB = person_DOB.strftime("%Y-%m-%d")

        for people in self.people:
            if person_name == people["person_name"] and person_DOB == people["person_DOB"].strftime("%Y-%m-%d") and active_phone_number == people["active_phone_number"]:
                return people

        return None

    def get_people_address(self, people=None, address=None):
        """Get people and address information."""
        result = []

        for relation in self.people_address:
            if people == relation["person_id"] or address == relation["address_id"]:
                result.append(relation)

        return result

    def insert_people(self, person_name=None, person_DOB=None, active_phone_number=None):
        """Insert people data."""

        # PARSE DATE OF BIRTH
        if type(person_DOB) == str:
            person_DOB = parse(person_DOB).strftime("%Y-%m-%d")
        elif type(person_DOB) in (date, datetime):
            person_DOB = datetime.strftime(person_DOB, "%Y-%m-%d")

        # SQL STATEMENT
        sql = f"""
        INSERT INTO people_master
            (person_name, person_DOB, active_phone_number)
        VALUES
            ('{person_name}', '{person_DOB}', '{active_phone_number}')
        """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

        # COMMIT SQL CHANGES
        self.connection.commit()

        # UPDATE PEOPLE DATA
        self.fetch_people()

        # RETURN NEW DATA
        return self.get_people(person_name=person_name, person_DOB=person_DOB, active_phone_number=active_phone_number)

    def insert_address(self, street_address=None, city=None, state=None, zip_code=None):
        """Insert address data."""

        # SQL STATEMENT
        sql = f"""
        INSERT INTO addresses
            (street_address, city, state, zip_code)
        VALUES
            ('{street_address}', '{city}', '{state}', '{zip_code}')
        """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

        # COMMIT SQL CHANGES
        self.connection.commit()

        # UPDATE ADDRESS DATA
        self.fetch_address()

        # RETURN NEW DATA
        return self.get_address(street_address=street_address, city=city, state=state, zip_code=zip_code)

    def insert_people_address(self, person_id=None, address_id=None, start_date=None, end_date=None):
        """Insert people address data."""

        # PARSE START DATE
        if type(start_date) == str:
            start_date = parse(start_date).strftime("%Y-%m-%d")
        elif type(start_date) in (date, datetime):
            start_date = datetime.strftime(start_date, "%Y-%m-%d")

        # PARSE END DATE ONLY IF END DATE NOT NULL
        if end_date:
            if type(end_date) == str:
                end_date = parse(end_date).strftime("%Y-%m-%d")
            elif type(end_date) in (date, datetime):
                end_date = datetime.strftime(end_date, "%Y-%m-%d")

            # SQL STATEMENT WITH END DATE
            sql = f"""
            INSERT INTO people_address
                (person_id, address_id, start_date, end_date)
            VALUES
                ('{person_id}', '{address_id}', '{start_date}', '{end_date}')
            """
        else:
            # SQL STATEMENT WITH NO END DATE
            sql = f"""
            INSERT INTO people_address
                (person_id, address_id, start_date)
            VALUES
                ('{person_id}', '{address_id}', '{start_date}')
            """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

        # COMMIT SQL CHANGES
        self.connection.commit()

        # UPDATE PEOPLE ADDRESS DATA
        self.fetch_people_address()

        # RETURN NEW DATA
        return self.get_people_address(people=person_id, address=address_id)

    def update_people_address(self, id=None, person_id=None, address_id=None, start_date=None, end_date=None):
        """Update people address data."""

        # PARSE START DATE
        if type(start_date) == str:
            start_date = parse(start_date).strftime("%Y-%m-%d")
        elif type(start_date) in (date, datetime):
            start_date = datetime.strftime(start_date, "%Y-%m-%d")

        # PARSE END DATE
        if type(end_date) == str:
            end_date = parse(end_date).strftime("%Y-%m-%d")
        elif type(end_date) in (date, datetime):
            end_date = datetime.strftime(end_date, "%Y-%m-%d")

        # SQL STATEMENT
        sql = f"""
        UPDATE people_address
            SET person_id = '{person_id}', address_id='{address_id}', start_date='{start_date}', end_date='{end_date}'
        WHERE
            id='{id}'
        """

        # EXECUTE SQL STATEMENT
        with self.connection.cursor() as cursor:
            cursor.execute(sql)

        # COMMIT SQL CHANGES
        self.connection.commit()

        # UPDATE PEOPLE ADDRESS DATA
        self.fetch_people_address

        # RETURN UPDATED DATA
        return self.get_people_address(people=person_id, address=address_id)


def display_menu():
    """Display Menu."""
    print("-" * 75)
    print("Choose option,")
    print("[1] -> Search contact information by last name.")
    print("[2] -> Search contact information by prefix.")
    print("[3] -> Search contact information by age range.")
    print("[4] -> Create contact information.")
    print("----")
    print("[0] -> QUIT.")
    print("-" * 75)


def capture_input():
    """Capture input from user."""
    return int(input("Option: "))


def main():
    address_book = AddressBook()
    print("-" * 75)
    print(address_book.__doc__)
    print(f"Made by {__author__}")
    print("-" * 75)

    try:
        # CONNECT TO DATABASE
        print("Connecting to database ...")
        print("-" * 75)
        host = input("Enter Host (localhost?): ")
        address_book.set_host(host)

        database = input("Enter Database: ")
        address_book.set_database(database)

        user = input("Enter User: ")
        address_book.set_user(user)

        password = getpass.getpass("Enter Password: ")

        address_book.connect(password=password)
        print("-" * 75)
        print("Connection successful\n")
        print("Host:\t ", address_book.host)
        print("Database:", address_book.database)
        print("User:\t ", address_book.user)
        print("-" * 75)

        while True:
            input("Press Enter to continue")

            # DISPLAY MENU
            display_menu()

            # GET INPUT FROM THE USER
            while True:
                try:
                    option = capture_input()
                except ValueError:
                    print("-" * 75)
                    print("Enter integer value.")
                    print("-" * 75)
                else:
                    break

            # PERFORM USER CHOSEN ACTION
            if option == 0:
                # EXIT
                print("-" * 75)
                print("Thank you for using this program.")
                print("-" * 75)
                sys.exit(0)
            elif option == 1:
                # SEARCH BY LAST NAME
                print("-" * 75)
                last__name = input("Enter last name: ")

                # GET ALL PEOPLE RECORD
                people = address_book.search_people(value=last__name)

                result = []

                # CREATE RECORD LIST
                for row in people:
                    last_name = row["person_name"].split(" ")[-1]
                    if last__name == last_name:
                        result.append(row)

                # PRINT OUT RESULTS
                print("-" * 75)
                if len(result) == 0:
                    print("No records found.")
                    print("-" * 75)
                else:
                    # HEADER
                    print("RECORDS")
                    print("-" * 75)
                    print("| Name", "\t\t|\t", "Address",
                          "\t|\t", "Active phone number")
                    print("-" * 75)

                    # BODY
                    for row in result:
                        # MANAGE DATA
                        name = row["person_name"]
                        phone = row["active_phone_number"]
                        address_relation = address_book.get_people_address(
                            people=row["person_id"])
                        address = "Not found"

                        for relation in address_relation:
                            if not relation["end_date"]:
                                for address_ in address_book.address:
                                    if address_["address_id"] == relation["address_id"]:
                                        address = address_["street_address"]
                        # DATA
                        print(f"| {name}", "\t|\t", address,
                              "\t|\t", phone)
                    print("-" * 75)
            elif option == 2:
                # SEARCH BY PREFIX
                print("-" * 75)
                prefix = input("Enter prefix: ")

                # GET ALL PEOPLE RECORD
                people = address_book.search_people(value=prefix)

                # PRINT OUT RESULTS
                print("-" * 75)
                if len(people) == 0:
                    print("No records found.")
                    print("-" * 75)
                else:
                    # HEADER
                    print("RECORDS")
                    print("-" * 75)
                    print("| Name", "\t\t|\t", "Date of Birth",
                          "\t|\t", "Active phone number")
                    print("-" * 75)

                    # BODY
                    for row in people:
                        # MANAGE DATA
                        name = row["person_name"]
                        phone = row["active_phone_number"]
                        dob = row["person_DOB"]

                        # DATA
                        print(f"| {name}", "\t|\t", dob, "\t|\t", phone)
                    print("-" * 75)
            elif option == 3:
                # SEARCH BY AGE RANGE
                print("-" * 75)
                while True:
                    try:
                        age = int(input("Enter age: "))
                    except ValueError:
                        print("Enter integer value.")
                    else:
                        break

                # GET ALL PEOPLE
                people = address_book.people
                result = []

                # FILTER RESULT
                for info in people:
                    if type(info["person_DOB"]) == str:
                        birth_year = parse(info["person_DOB"])
                    elif type(info["person_DOB"]) in (date, datetime):
                        birth_year = info["person_DOB"].year
                    else:
                        raise TypeError("Unknown DOB type.")

                    # (THIS YEAR - Person's birth year) == age?
                    if (THIS_YEAR - birth_year) == age:
                        result.append(info)

                # PRINT OUT RESULTS
                print("-" * 75)
                if len(result) == 0:
                    print("No records found.")
                    print("-" * 75)
                else:
                    # HEADER
                    print("RECORDS")
                    print("-" * 75)
                    print("| Name", "\t\t|\t", "Date of Birth",
                          "\t|\t", "Active phone number")
                    print("-" * 75)

                    # BODY
                    for row in result:
                        # MANAGE DATA
                        name = row["person_name"]
                        phone = row["active_phone_number"]
                        dob = row["person_DOB"]

                        # DATA
                        print(f"| {name}", "\t|\t", dob, "\t|\t", phone)
                    print("-" * 75)
            elif option == 4:
                # ADD CONTACT
                print("-" * 75)
                print("To create/update contact,")
                print("-" * 75)
                name = input("Enter full name: ")
                phone = input("Enter active phone number: ")
                dob = input("Enter date of birth (MM/DD/YYYY): ")

                # PARSE DATE
                dob_parsed = parse(dob)

                # FETCH PEOPLE RECORD
                people_record = address_book.get_people(
                    person_name=name, person_DOB=dob_parsed, active_phone_number=phone)

                print("-" * 75)

                # ADDRESS INFORMATION
                street_address = input("Enter street address: ")
                city = input("Enter city name: ")
                state = input("Enter state name: ")
                zip_code = input("Enter zip code: ")

                # FETCH ADDRESS RECORD
                address_record = address_book.get_address(
                    street_address=street_address, city=city, state=state, zip_code=zip_code)
                # CREATE NEW ADDRESS RECORD IF NOT FOUND
                if not address_record:
                    address_record = address_book.insert_address(
                        street_address=street_address, city=city, state=state, zip_code=zip_code)
                print("-" * 75)

                if people_record:
                    # UPDATE EXISTING RECORD
                    print(
                        "Existing contact record found!\n\rPrevious contact address information will be updated.")

                    # MAKE PREVIOUS RECORDS HISTORIC
                    people_address_record = address_book.get_people_address(
                        people=people_record["person_id"])
                    for record in people_address_record:
                        if not record["end_date"]:  # UPDATE ONLY IF END DATE IS NULL
                            address_book.update_people_address(
                                id=record["id"], person_id=record["person_id"], address_id=record["address_id"], start_date=record["start_date"], end_date=datetime.now())
                else:
                    # CREATE NEW RECORD IF NOT FOUND
                    people_record = address_book.insert_people(
                        person_name=name, person_DOB=dob_parsed, active_phone_number=phone)
                    print("New contact information record created!")

                # CREATE NEW PEOPLE ADDRESS RECORD
                address_book.insert_people_address(
                    person_id=people_record["person_id"], address_id=address_record["address_id"], start_date=datetime.now())
                print("-" * 75)
            else:
                print("-" * 75)
                print("Enter valid option.")
                print("-" * 75)
    except KeyboardInterrupt:
        # EXIT
        print("")
        print("-" * 75)
        print("Thank you for using this program.")
        print("-" * 75)
        sys.exit(0)
    except Exception as e:
        # EXIT AFTER EXCEPTION
        print("")
        print("-" * 75)
        print("Error: ", str(e))
        print("-" * 75)
        sys.exit(0)


if __name__ == "__main__":
    main()
