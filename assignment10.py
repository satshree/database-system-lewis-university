import sys
import sqlalchemy as db
from assignment import AddressBook, main
from dateutil.parser import parse
from datetime import date, datetime


class AddressBookORM(AddressBook):
    # OVERRIDE
    def connect(self, host=None, user=None, password=None, database=None):
        try:
            # CONNECT TO DATABASE
            self.engine = db.create_engine(
                f"mysql://{user}:{password}@{host}/{database}", echo=False)
            self.connection = self.engine.connect()
            self.metadata = db.MetaData()
        except Exception as e:
            print("-" * 75)
            print("Error:", str(e))
            print("-" * 75)
            sys.exit(1)
        else:
            self.fetch_people()
            self.fetch_address()
            self.fetch_people_address()

        return True

    def fetch_people(self):
        # GET TABLE
        self.people_table = db.Table(
            'people_master', self.metadata, autoload=True, autoload_with=self.engine)

        # SQL QUERY
        query = db.select(self.people_table)

        # EXECUTE SQL QUERY
        execute = self.connection.execute(query)

        # FETCH QUERY RESULT
        all_people = execute.fetchall()

        self.people = [{
            "person_id": people[0],
            "person_name": people[1],
            "person_DOB": people[2],
            "active_phone_number": people[3],
        } for people in all_people]

    def fetch_address(self):
        # GET TABLE
        self.address_table = db.Table(
            'addresses', self.metadata, autoload=True, autoload_with=self.engine)

        # SQL QUERY
        query = db.select(self.address_table)

        # EXECUTE SQL QUERY
        execute = self.connection.execute(query)

        # FETCH QUERY RESULT
        all_address = execute.fetchall()

        self.address = [{
            "address_id": address[0],
            "street_address": address[1],
            "city": address[2],
            "state": address[3],
            "zip_code": address[4],
        } for address in all_address]

    def fetch_people_address(self):
        # GET TABLE
        self.people_address_table = db.Table(
            'people_address', self.metadata, autoload=True, autoload_with=self.engine)

        # SQL QUERY
        query = db.select(self.people_address_table)

        # EXECUTE SQL QUERY
        execute = self.connection.execute(query)

        # FETCH QUERY RESULT
        all_people_address = execute.fetchall()

        self.people_address = [{
            "id": people_address[0],
            "person_id": people_address[1],
            "address_id": people_address[2],
            "start_date": people_address[3],
            "end_date": people_address[4],
        } for people_address in all_people_address]

    def insert_people(self, person_name=None, person_DOB=None, active_phone_number=None):
        # PARSE DATE OF BIRTH
        if type(person_DOB) == str:
            person_DOB = parse(person_DOB).strftime("%Y-%m-%d")
        elif type(person_DOB) in (date, datetime):
            person_DOB = datetime.strftime(person_DOB, "%Y-%m-%d")

        # SQL STATEMENT
        sql = self.people_table.insert().values(
            person_name=person_name,
            person_DOB=person_DOB,
            active_phone_number=active_phone_number,
        )

        # EXECUTE SQL STATEMENT
        self.connection.execute(sql)

        # UPDATE PEOPLE
        self.fetch_people()

        # RETURN NEW LIST
        return self.get_people(
            person_name=person_name,
            person_DOB=person_DOB,
            active_phone_number=active_phone_number,
        )

    def insert_address(self, street_address=None, city=None, state=None, zip_code=None):
        # SQL STATEMENT
        sql = self.people_table.insert().values(
            street_address=street_address,
            city=city,
            state=state,
            zip_code=zip_code
        )

        # EXECUTE SQL STATEMENT
        self.connection.execute(sql)

        # UPDATE ADDRESS
        self.fetch_address()

        # RETURN NEW LIST
        return self.get_address(
            street_address=street_address,
            city=city,
            state=state,
            zip_code=zip_code
        )

    def insert_people_address(self, person_id=None, address_id=None, start_date=None, end_date=None):
        # PARSE START DATE
        if type(start_date) == str:
            start_date = parse(start_date).strftime("%Y-%m-%d")
        elif type(start_date) in (date, datetime):
            start_date = datetime.strftime(start_date, "%Y-%m-%d")

        # PARSE END DATE ONLY IF END DATE NOT NULL
        if end_date:
            if type(end_date) == str:
                end_date = parse(end_date).strftime("%Y-%m-%d")
            elif type(end_date) in (date, datetime):
                end_date = datetime.strftime(end_date, "%Y-%m-%d")

        # SQL STATEMENT
        sql = self.people_address_table.insert().values(
            person_id=person_id,
            address_id=address_id,
            start_date=start_date,
            end_date=end_date
        )

        # EXECUTE SQL STATEMENT
        self.connection.execute(sql)

        # UPDATE PEOPLE ADDRESS
        self.fetch_people_address()

        # RETURN NEW LIST
        return self.get_people_address(
            person_id=person_id,
            address_id=address_id,
            start_date=start_date,
            end_date=end_date
        )

    def update_people_address(self, id=None, person_id=None, address_id=None, start_date=None, end_date=None):
        # PARSE START DATE
        if type(start_date) == str:
            start_date = parse(start_date).strftime("%Y-%m-%d")
        elif type(start_date) in (date, datetime):
            start_date = datetime.strftime(start_date, "%Y-%m-%d")

        # PARSE END DATE
        if type(end_date) == str:
            end_date = parse(end_date).strftime("%Y-%m-%d")
        elif type(end_date) in (date, datetime):
            end_date = datetime.strftime(end_date, "%Y-%m-%d")

        # SQL STATEMENT
        sql = self.people_address_table.update().where(self.people_address_table.id == id).values(
            person_id=person_id,
            address_id=address_id,
            start_date=start_date,
            end_date=end_date
        )

        # EXECUTE SQL STATEMENT
        self.connection.execute(sql)

        # UPDATE PEOPLE ADDRESS
        self.fetch_people_address()

        # RETURN NEW LIST
        return self.get_people_address(
            people=person_id,
            address=address_id
        )


if __name__ == "__main__":
    main()
